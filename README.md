# Yii 2 Templates

Este proyecto contiene templates personalizados, del Gii Generator.



para instalarlo ir la carpeta frontend  y ejecutar el siguiente comando
```php
git clone https://gitlab.com/linuxzero/yii-2-templates.git templates
```
Agregar la siguiente linea en el archivo main-local.php

```php
if (YII_ENV_DEV) {    
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',      
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],  
        'generators' => [ //here
            'model' => [ // generator name
                'class' => 'yii\gii\generators\model\Generator', // generator class
                'templates' => [ //setting for out templates
                    'myModel' => '@frontend/templates/model/default', // template name => path to template
                ]
            ]
        ],
    ];
}
```